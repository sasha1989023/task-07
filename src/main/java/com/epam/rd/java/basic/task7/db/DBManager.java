package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class DBManager {

    private static final Lock CONNECTION_LOCK = new ReentrantLock();
    private static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    private static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    private static final String FIND_ALL_USERS = "SELECT * FROM users";
    private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String FIND_USER_LOGIN = "SELECT * FROM users WHERE login= ?";
    private static final String FIND_TEAM_LOGIN = "SELECT * FROM teams WHERE name= ?";
    private static final String INSERT_USER_TEAM = "INSERT INTO users_teams VALUES (?, ?)";
    private static final String FIND_TEAM_USER_ID = "SELECT t.id, t.name FROM users_teams ut\n"
            + "JOIN users u ON ut.user_id = u.id\n"
            + "JOIN teams t ON ut.team_id = t.id\n"
            + "WHERE u.id = ?";
    private static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    private static final String UPDATE_TEAM = "UPDATE teams SET name= ? WHERE id= ?";
    private static final String DELETE_USER = "DELETE FROM users WHERE login=?";
    private static DBManager instance;
    private static Connection connection;

    public static Connection getConnection() throws SQLException {
        Properties props = new Properties();

        try (InputStream in = Files.newInputStream(Paths.get("app.properties"))) {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String url = props.getProperty("connection.url");
        String username = props.getProperty("user");
        String password = props.getProperty("password");

        return DriverManager.getConnection(url, username, password);
    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            try {
                instance = new DBManager();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private DBManager() throws SQLException {
        connection = getConnection();
    }

    public List<User> findAllUsers() throws DBException {
        Statement statement = null;
        ResultSet resultSet = null;

        List<User> users = new ArrayList<>();
        try {
            CONNECTION_LOCK.lock();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(FIND_ALL_USERS);

            while (resultSet.next()) {
                User user = new User();
                users.add(user);
                user.setId(resultSet.getInt(1));
                user.setLogin(resultSet.getString(2));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(statement);
            CONNECTION_LOCK.unlock();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, user.getLogin());

            if (preparedStatement.executeUpdate() != 1) {
                return false;
            }

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                int idField = resultSet.getInt(1);
                user.setId(idField);
            }
            connection.commit();
        } catch (Exception e) {
            rollback();
            throw new DBException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = null;

        try {
            CONNECTION_LOCK.lock();
            preparedStatement = connection.prepareStatement(FIND_USER_LOGIN);
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Team team = null;

        try {
            CONNECTION_LOCK.lock();
            preparedStatement = connection.prepareStatement(FIND_TEAM_LOGIN);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        Statement statement = null;
        ResultSet resultSet = null;
        List<Team> teams = new ArrayList<>();

        try {
            CONNECTION_LOCK.lock();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(FIND_ALL_TEAMS);

            while (resultSet.next()) {
                Team team = new Team();
                teams.add(team);
                team.setId(resultSet.getInt(1));
                team.setName(resultSet.getString(2));
            }
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(statement);
            CONNECTION_LOCK.unlock();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, team.getName());

            if (preparedStatement.executeUpdate() != 1) {
                return false;
            }

            resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                int idField = resultSet.getInt(1);
                team.setId(idField);
            }
            connection.commit();
        } catch (Exception e) {
            rollback();
            throw new DBException(e.getMessage(), e);
        } finally {
            close(resultSet);
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        PreparedStatement preparedStatement = null;

        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT_USER_TEAM);

            for (Team t : teams) {
                preparedStatement.setInt(1, user.getId());
                preparedStatement.setInt(2, t.getId());
                preparedStatement.addBatch();
            }

            int[] usersTeams = preparedStatement.executeBatch();

            for (int i : usersTeams) {
                if (i != 1) {
                    return false;
                }
            }

            connection.commit();
            return true;
        } catch (SQLException ex) {
            rollback();
            throw new DBException(ex.getMessage(), ex);
        } finally {
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Team> teams = new ArrayList<>();

        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement(FIND_TEAM_USER_ID);
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();

            while (rs.next()) {
                Team team = new Team();
                teams.add(team);
                team.setId(rs.getInt(1));
                team.setName(rs.getString(2));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            close(rs);
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        PreparedStatement preparedStatement = null;

        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(DELETE_TEAM);
            preparedStatement.setString(1, team.getName());

            if (preparedStatement.executeUpdate() != 1) {
                return false;
            }
            connection.commit();
        } catch (SQLException e) {
            rollback();
            throw new DBException(e.getMessage(), e);
        } finally {
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        for (User user : users) {
            deleteOneUser(user);
        }
        return true;
    }

    public boolean deleteOneUser(User user) throws DBException {
        PreparedStatement preparedStatement = null;

        try {
            CONNECTION_LOCK.lock();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(DELETE_USER);
            preparedStatement.setString(1, user.getLogin());

            if (preparedStatement.executeUpdate() != 1) {
                return false;
            }
            connection.commit();
        } catch (SQLException e) {
            rollback();
            throw new DBException(e.getMessage(), e);
        } finally {
            close(preparedStatement);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        PreparedStatement ps = null;

        try {
            CONNECTION_LOCK.lock();
            ps = connection.prepareStatement(UPDATE_TEAM);
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());

            if (ps.executeUpdate() != 1) {
                return false;
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        } finally {
            close(ps);
            CONNECTION_LOCK.unlock();
        }
        return true;
    }

    private static void close(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void rollback() throws DBException {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            throw new DBException(ex.getMessage(), ex);
        }
    }

}
